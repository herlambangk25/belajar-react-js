import { IconBrandFacebook, IconBrandGithub, IconBrandTwitter } from '@tabler/icons-react';
import { useState } from 'react';

import Button from './components/Button';
import Card from './components/Card';
import Counter from './components/Counter';
import Input from './components/input';
import Label from './components/label';
import PlaceContentCenter from './components/PlaceContentCenter';
import Todo from './components/Todo';

function App() {
    return (
        <PlaceContentCenter>
            <Todo />
        </PlaceContentCenter>
    );
}

export default App;
