import { IconBrandFacebook, IconBrandGithub, IconBrandTwitter } from '@tabler/icons-react';
import { useState } from 'react';

import Button from './components/Button';
import Card from './components/Card';
import Counter from './components/Counter';
import Input from './components/input';
import Label from './components/label';
import PlaceContentCenter from './components/PlaceContentCenter';

function App() {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [form, setForm] = useState({
        name: '',
        email: '',
    });

    function onChange(event) {
        setForm({
            ...form,
            [event.target.name]: event.target.value,
        });
    }

    function Submit(event) {
        event.preventDefault();
        console.log(form);
    }

    return (
        <PlaceContentCenter>
            <Card>
                <Card.Title> Sign up For new account !</Card.Title>

                <form onSubmit={Submit}>
                    <Card.Body>
                        <div className='mb-5 border rounded-lg p-4'>
                            <p>Name: {form.name || '----'}</p>
                            <p>Email: {form.email || '----'}</p>
                        </div>
                        <div className='mb-6'>
                            <Label htmlFor='name' value={'Name'} />
                            <Input value={form.name} onChange={onChange} id={'name'} name={'name'} />
                        </div>
                        <div className=''>
                            <Label htmlFor='email' value={'Email'} />
                            <Input value={form.email} onChange={onChange} id={'email'} name={'email'} />
                        </div>
                    </Card.Body>
                    <Card.Footer>
                        <Button>
                            <IconBrandGithub />
                            Register
                        </Button>
                    </Card.Footer>
                </form>
            </Card>
        </PlaceContentCenter>
    );
}

export default App;
