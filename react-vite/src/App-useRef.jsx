import { useRef, useState } from 'react';
import Button from './components/Button';
import Card from './components/Card';
import Input from './components/input';
import PlaceContentCenter from './components/PlaceContentCenter';

function App() {
    // const inputRef = useRef(null);
    // const [tick, setTick] = useState(0);
    function handleClick() {
        // inputRef.current.focus();
        // console.log(inputRef);
        // tick.current = tick.current + 1;
        // console.log(tick.current);
        // const nextTick = tick + 1;
        // setTick(nextTick);
        // console.log(nextTick);
    }
    return (
        <PlaceContentCenter>
            <Card>
                <Card.Title>useRef Hooks</Card.Title>
                <Card.Body>
                    <Input placeholder={'name'} isFocus className='border border-slate-500' />
                    <Input placeholder={'email'} className='border border-slate-500' />
                    <Button onClick={handleClick}>Tick</Button>
                </Card.Body>
                {/* <Card.Footer>You clicked {tick.current} Times</Card.Footer> */}
            </Card>
        </PlaceContentCenter>
    );
}

export default App;
